;; (setq show-paren-style 'expression)
(show-paren-mode 1)
(display-time-mode 1)
;;main modes
(menu-bar-mode -1) ; disable menu bar
(global-hl-line-mode 1) ; higlight current line
(set-face-background 'hl-line "#3e4446") ; higlight color
(set-face-foreground 'highlight nil) ; keep syntax highlighting in the current line
(tool-bar-mode -1) ; disable toll bar
(scroll-bar-mode -1) ; disable scrollbars
(desktop-save-mode 1) ; save current session
(electric-pair-mode 1) ; auto close brackets 
(setq make-backup-files nil) ; Don't want any backup files
(setq auto-save-list-file-name nil) ; Don't want any .saves files
(setq auto-save-default nil) ; Don't want any auto saving
(set-face-attribute 'default nil :height 110)

;; main include path
(add-to-list 'load-path "~/.emacs.d")

;; line numbers
(require 'linum+)
(setq linum-format "%d ")
(global-linum-mode 1)

(setq linum-mode-inhibit-modes-list '(eshell-mode
                                      shell-mode
                                      erc-mode
                                      jabber-roster-mode
                                      jabber-chat-mode
                                      gnus-group-mode
                                      gnus-summary-mode
                                      gnus-article-mode))

(defadvice linum-on (around linum-on-inhibit-for-modes)
  "Stop the load of linum-mode for some major modes."
    (unless (member major-mode linum-mode-inhibit-modes-list)
      ad-do-it))

(ad-activate 'linum-on)
(require 'uniquify)
(setq uniquify-buffer-name-style 'reverse)

(setq frame-title-format
      (list (format "%s %%S: %%j " (system-name))
        '(buffer-file-name "%f" (dired-directory dired-directory "%b"))))


;; intelectual file opening
(require 'ido)
(ido-mode t)
(setq ido-enable-flex-matching t)

;; file/functions tree
(require 'sr-speedbar)
(global-set-key (kbd "<f12>") 'sr-speedbar-toggle)
;; (custom-set-variables
;;  '(speedbar-show-unknown-files t)
;; )
(setq speedbar-use-images nil)
(add-hook 'speedbar-before-popup-hook (lambda () (linum-mode -1)))
(speedbar-add-supported-extension ".module")
(speedbar-add-supported-extension ".install")
(add-to-list 'speedbar-fetch-etags-parse-list
    '("\\.module" . speedbar-parse-phptag)
    '("\\.install" . speedbar-parse-phptag))

;; buffers
(require 'bs)
(setq bs-configurations
'(("files" "^\\*scratch\\*" nil nil bs-visits-non-file bs-sort-buffer-interns-are-last)))
(global-set-key (kbd "<f2>") 'bs-show)

;; additional repos for packages
(require 'package)
(setq package-archives '(("ELPA" . "http://tromey.com/elpa/")
                          ("gnu" . "http://elpa.gnu.org/packages/")
                          ("marmalade" . "http://marmalade-repo.org/packages/")))


(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")
(ac-config-default)


(require 'php-mode)
(add-to-list 'auto-mode-alist '("\\.module$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.inc$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.install$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.engine$" . php-mode))

(add-hook 'php-mode-hook 'my-php-mode-hook)
(defun my-php-mode-hook ()
  "My PHP mode configuration."
  (setq indent-tabs-mode nil
        tab-width 2
        c-basic-offset 2))
(setq-default indent-tabs-mode nil)
(setq js-indent-level 2)
(setq css-indent-offset 2)


;; (define-derived-mode drupal-mode php-mode "Drupal"
;;   "Major mode for Drupal coding.\n\n\\{drupal-mode-map}"
;;   (setq c-basic-offset 2)
;;   (setq indent-tabs-mode nil)
;;   (setq fill-column 78)
;;   (setq show-trailing-whitespace t)
;;   (add-hook 'before-save-hook 'delete-trailing-whitespace)
;;   (c-set-offset 'case-label '+)
;;   (c-set-offset 'arglist-close 0)
;;   (c-set-offset 'arglist-intro '+) ; for FAPI arrays and DBTNG
;;   (c-set-offset 'arglist-cont-nonempty 'c-lineup-math) ; for DBTNG fields and values
;;   (run-mode-hooks 'drupal-mode-hook)
;; )
;; (provide 'drupal-mode)

;; Color theme
(add-to-list 'load-path "~/.emacs.d/color-theme/")
(require 'color-theme)
(color-theme-initialize)
(setq color-theme-is-global t)
(load-file "~/.emacs.d/color-theme-monokai.el")
(color-theme-monokai)

(require 'twig-mode)

;;Bookmarks
(global-set-key (kbd "<f5>") 'bookmark-set)
(global-set-key (kbd "<f6>") 'bookmark-jump)
(global-set-key (kbd "<f4>") 'bookmark-bmenu-list)

(require 'web-mode) 
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode)) 
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[gj]sp\\'" . web-mode)) 
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode)) 
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode)) 
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode)) 
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))

;;Folding
(defvar hs-special-modes-alist
(mapcar 'purecopy
'((c-mode "{" "}" "/[*/]" nil nil)
(c++-mode "{" "}" "/[*/]" nil nil)
(bibtex-mode ("@\\S(*\\(\\s(\\)" 1))
(java-mode "{" "}" "/[*/]" nil nil)
(js-mode "{" "}" "/[*/]" nil)
(php-mode "{" "}" "/[*/]" nil)
(emacs-lisp- "(" ")" nil))))

(require 'hideshow)

;; (setq exec-path (cons (expand-file-name "~/.gem/ruby/1.8/bin") exec-path))

(add-to-list 'load-path (expand-file-name "~/.emacs.d/"))
(setq scss-sass-command "cd ../ && compass compile")
(autoload 'scss-mode "scss-mode")
(add-to-list 'auto-mode-alist '("\\.scss\\'" . scss-mode))

;; multiple cursors sublime like
(add-to-list 'load-path (expand-file-name "~/.emacs.d/multiple-cursors/"))
(require 'multiple-cursors)
(global-set-key (kbd "C-c C-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

;; keys

(global-set-key (kbd "C-z") 'undo)
(global-unset-key "\M-j")

(global-set-key (kbd "<f9>") 'hs-toggle-hiding)
(global-set-key (kbd "C-<f9>") 'hs-hide-all)
(global-set-key (kbd "C-S-<f9>") 'hs-show-all)

;; make cursor movement keys under right hand's home-row.
;; (global-set-key (kbd "M-i") 'previous-line)
;; (global-set-key (kbd "M-j") 'backward-char)
;; (global-set-key (kbd "M-k") 'next-line)
;; (global-set-key (kbd "M-l") 'forward-char)

;; (global-set-key (kbd "M-u") 'backward-word)
;; (global-set-key (kbd "M-o") 'forward-word)

(defun duplicate-line()
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (open-line 1)
  (next-line 1)
  (yank)
)
(global-set-key (kbd "C-S-d") 'duplicate-line)
(global-set-key "%" 'match-paren)
          
(defun match-paren (arg)
  "Go to the matching paren if on a paren; otherwise insert %."
  (interactive "p")
  (cond ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
	((looking-at "\\s\)") (forward-char 1) (backward-list 1))
	(t (self-insert-command (or arg 1)))))
