;;; color-theme-molokai-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (color-theme-molokai) "color-theme-molokai" "color-theme-molokai.el"
;;;;;;  (21393 32678 607762 12000))
;;; Generated autoloads from color-theme-molokai.el

(autoload 'color-theme-molokai "color-theme-molokai" "\
Color theme based on the Molokai color scheme for vim.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("color-theme-molokai-pkg.el") (21393 32678
;;;;;;  688203 600000))

;;;***

(provide 'color-theme-molokai-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; color-theme-molokai-autoloads.el ends here
